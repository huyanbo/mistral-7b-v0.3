# 使用此代码库的方法
在使用mistral_npu.py去npu上使用mistral-7b-v0.3模型时，需要事先先安装好python，本人使用的3.10版本。numpy依赖库，不要使用2.0及以上的版本，本人使用的1.23版本。
再去安装CANN、pytorch、torch_npu。

然后安装requirment.txt文件里列出的依赖，可以使用`pip install -r`命令。

依赖全部安装完成后就可以体验mistral-7b-v0.3的功能了。不要忘了把模型路径改成你的模型路径，npu设置为你的npu信息。

**下载模型请参考代码库mian分支README文件**

运行demo:
切换到你部署好的虚拟环境，进入demo文件所在文件夹，使用以下命令`python mistral_npu.py`

# 详细步骤：
先确定设备版本，我的设备是CANN 8.0.RC2

创建conda虚拟环境，python版本3.10
下载numpy 1.23版本

前往https://modelscope.cn/models/robert/Mistral-7B-v0.3/files下载模型，先使用：`pip install modelscope` 安装modelscope，再使用：
`modelscope download --model LLM-Research/Mistral-7B-Instruct-v0.3 --local_dir ./dir` 把模型下载到当前文件夹。

前往https://gitcode.com/huyanbo/mistral-7b-v0.3/overview拉取代码，使用：`git clone https://gitcode.com/huyanbo/mistral-7b-v0.3.git`，

修改mistral_npu.py文件里的torch_device为你的npu设备，把DEFAULT_CKPT_PATH参数后面的模型路径修改为你的模型路径

参考：https://github.com/Ascend/pytorch/blob/master/README.zh.md安装torch_npu。使用：`pip3 install torch==2.2.0` 安装pytorch，使用：`pip3 install pyyaml` 和`pip3 install setuptools` 安装torch_npu依赖，再使用`pip3 install torch-npu==2.2.0` 安装torch_npu。

安装模型依赖：
进入之前拉取的代码的文件夹，使用`pip install -r requirment.txt `安装依赖

重新下载numpy==1.23.5
最后使用`python mistral_npu.py运行模型`